<?php

/* FE - Script para obtener informacion de marketing
 * @param Limit (ini fin) 
 * php /var/www/html/triggers/migracion/marketing/scriptMarketingExecuteAll.php customers 0 40000 ("sin limite" - -) 2017(4) || 2017-12-01(10)("sin año" -) commerce || marketing || vacio(ambos)
 * Separador ~ sin "
 */

require_once dirname(__FILE__) . '/scriptMarketing.php';
require_once(dirname(__FILE__).'/Daomail.php');;

class scriptMarketingCustomers extends scriptMarketing {

    public $_flagCompany = true;
    public $_flagArea = true;
    public $_flagCommerce = true;
    public $_customerData = '';
        
/*lbayona  4823 hybris  inicio*/
public   function  eliminarletras($string){

$search= array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");

$replace = array('');

 $string=str_replace($search, $replace, $string);

return $string;

}
/*lbayona 4823 hybris  fin*/
    public function getCustomers($from, $to, $date = false, $source = false) {
          $Daomail= new Daomail();
        $this->_source = $source;
        $this->init();
        echo "     -----------Inicia Ejecucion " . get_class($this) . "-----------   \n";
        Mage::log("     -----------Inicia Ejecucion " . get_class($this) . "-----------   ", null, 'scriptMarketingCustomers.log');
                
        $this->_limit . "\n";        
        $aditional = '';
        if ($date) {
            $aditional = " AND e.created_at >= '" . $date . "' ";
        }
        $corte = '';
        if($this->_corte != ''){
            $corte = " AND e.updated_at > '" . $this->_corte . "'";
        }
        if ($from != '' && $to != '') {
            $this->_limit = 'LIMIT ' . $from . ',' . $to;
        }
        try { 
            $i = 1;
            $currentProcessDate = date('Y-m-d H:i:s', time());
            //$sql = "SELECT `e`.*,w.`name` AS website FROM `customer_entity` AS `e` INNER JOIN `core_website` AS w ON e.`website_id` = w.`website_id` AND e.`website_id` IN (" . $this->_websites . ") WHERE (e.entity_type_id = '1')" . $aditional . " ORDER BY e.entity_id " . $this->_limit . ";";
          $sql = "SELECT `e`.* FROM `customer_entity` AS `e` WHERE e.`website_id` IN (" . $this->_websites . ") AND (e.entity_type_id = '1')" . $aditional . $corte . " ORDER BY e.entity_id " . $this->_limit . ";";

            $resSql = $this->_connection->fetchAll($sql);
            $res = array();
            $customRes = array();
            
           

   
   
            foreach ($resSql as $customer) {
                if ($this->_source == 'commerce') {

                    if ($this->_customers_processed[$customer['entity_id']]) {
                        if (strtotime($customer['updated_at']) > strtotime($this->_customers_processed[$customer['entity_id']]['fecha'])) {                            
                            $customRes[] = $customer;
                        }
                    } else {
                        $customRes[] = $customer;
                    }
                    unset($customer);                    
                } else if ($this->_source == 'marketing') {
                    
                    if ($this->_customerData[$customer['entity_id']]) {

                        if (strtotime($customer['updated_at']) > strtotime($this->_customerData[$customer['entity_id']]['fecha'])) {                            
                            $customRes[] = $customer;
                            }
                    } else {
                        $customRes[] = $customer;
                       
                    }
                    unset($customer);
                    
                }
            }
            $res = $customRes;
            //print_r($res);  
            $total = count($res);
            Mage::log("#Datos a procesar: " . get_class($this) . " - " . $total, null, 'scriptMarketingCustomers.log');
            if(count($res) == 0){
                echo "\n-----------No Data-----------";
                echo "\n-----------Fin de Ejecucion " . get_class($this) . "-----------\n";
                return false;
                Mage::log("     -----------No Data-----------   ", null, 'scriptMarketingCustomers.log');
                Mage::log("     -----------Fin de Ejecucion " . get_class($this) . "-----------   ", null, 'scriptMarketingCustomers.log');
            }

         
           
            foreach ($res as $customer) {         
                        
                $collection = Mage::getModel('customer/customer')->load($customer['entity_id']);
             
         

            $address = $collection->getDefaultBillingAddress();
               /*lbayona   4823 hybris  inicio */
            if(!$address){      
                $address1 = $collection->getAddresses();
                $paso = true;
                $address = null;
                foreach ($address1 as $addressItem) {
                    if($paso){
                        $address = $addressItem;
                        //echo"hola yo address".$addressItem->getId();
                        $paso= false;
                    }
                }
            }
            /*lbayona   4823 hybris  fin */
            
     /*lbayona   4823 hybris  inicio */
     
               if (isset($address)) {

/*lbayona   4823 hybris  fin */

       

          
          
                    $this->setDataCustomers($collection->getData(), $address->getData());
                } else {
              
               
                    $this->setDataCustomers($collection->getData(), $collection->getData());
                }
                //echo "\n" . $i;
                echo "\n" . ($i / $total) * 100 . "%";
                $i++;
            }             
            //$this->putTxt($this->_dir, $this->_fileNameTxt,$this->_customerData);                                    
            if ($this->_source == 'commerce') {                
                $this->putTxt($this->_dirCommerce, $this->_commerceFileNameTxt,serialize($this->_customers_processed),$currentProcessDate);            
            } else if ($this->_source == 'marketing') {
               
                //$this->putTxt($this->_dir, $this->_fileNameTxt,$this->_customerData);
                $this->putTxt($this->_dir, $this->_fileNameTxt,serialize($this->_customerData),$currentProcessDate);            
            }
        } catch (Exception $e) {
            Mage::log("Error  - " . $e->getMessage(), null, 'scriptMarketingError.log');
            Mage::log("     -----------Fin de Ejecucion " . get_class($this) . "-----------   ", null, 'scriptMarketingError.log');
            echo "Error: " . $e->getMessage();
        }

      
        $current = date('d.m.Y', Mage::getModel('core/date')->timestamp(time()));
         $fileNameCompany = 'InteractionContact_Company_MAGENTO_' . $current . '.csv';
            $fileNameArea = 'MarketingPermission_MAGENTO_' . $current . '.csv';
          
              
        $fileName = 'InteractionContact_Person_MAGENTO_' . $current . '.csv';
    if(file_exists('/var/www/html/var/export/migracion/'.$fileName.'')){


      try{  
            $Iname="InteractionContact_Person_MAGENTO";
            
            $Daomail->enviar_archivoexcel($Iname);
             echo exec('scp /var/www/html/var/export/migracion/'.$fileName.' marketing:/home/ubuntu/sandra/Interaction_Contact/1_MAGENTO');
 
        }catch(Exception $e){
           Mage::log("Error  - " . $e->getMessage(), null, 'scriptMarketingError.log');
        }
}else{
echo "error";

}


if(file_exists('/var/www/html/var/export/migracion/'.$fileNameCompany.'')){


      try{ 
             
   //echo  $fileNameCompany;
       
                    $company="InteractionContact_Company_MAGENTO";
                    $Daomail->enviar_archivoexcel($company);
            echo exec('scp /var/www/html/var/export/migracion/'.$fileNameCompany.' marketing:/home/ubuntu/sandra/Interaction_Contact/1_MAGENTO');
 
        }catch(Exception $e){
           Mage::log("Error  - " . $e->getMessage(), null, 'scriptMarketingError.log');
        }
}else{
echo "error";

}


if(file_exists('/var/www/html/var/export/migracion/'.$fileNameArea.'')){


      try{ 
             
   //echo  $fileNameCompany;
        
                    $permission="MarketingPermission_MAGENTO";
                    $Daomail->enviar_archivoexcel($permission);
            echo exec('scp /var/www/html/var/export/migracion/'.$fileNameArea.' marketing:/home/ubuntu/sandra/Marketing_permissions');
 
        }catch(Exception $e){
           Mage::log("Error  - " . $e->getMessage(), null, 'scriptMarketingError.log');
        }
}else{
echo "error";

}





    Mage::log("     -----------Fin de Ejecucion " . get_class($this) . "-----------   ", null, 'scriptMarketingCustomers.log');
        echo "\n     -----------Fin de Ejecucion " . get_class($this) . "-----------   \n";
       }

      

    private function setDataCustomers($customer, $address) {
      
        if ($customer) {
            $firstname = '';
            $middlename = '';
            $lastname = '';
            $mothersname = '';
            $middlename = '';
            $company_name = '';
            $idType = '';
            $longitud = '';
            $latitud = '';
            $company = false;
            $celular = '';
            $telephone = '';
            if ($address['cardtype'] && $address['cardtype'] != '1320') {
                $company = false;
                $firstname = trim($address['firstname']);
                $middlename = trim($address['middlename']);
                $lastname = trim($address['lastname']);
                $mothersname = trim($address['mothersname']);                
            } else if ($address['cardtype'] == '1320') {
                $company = true;
                if ($address['socialreason'] != '') {
                    $company_name = mb_strtolower(trim($address['socialreason']), "UTF-8");
                    if ($address['socialreasontype'] != '' || $address['socialreasontype'] != '.' || $address['socialreasontype'] != '..') {
                        $company_name .= ' ' . trim($address['socialreasontype']);
                    }
                } else if ($address['firstname'] != '') {
                    $company_name = mb_strtolower(trim($address['firstname']), "UTF-8");
                    if ($address['lastname'] != '') {
                        $company_name .= ' ' . trim($address['lastname']);
                    }
                }
            } else if (!$customer['tid'] || $customer['tid'] != '222') {
                $company = false;
                $firstname = trim($customer['firstname']);
                $middlename = trim($customer['middlename']);
                $lastname = trim($customer['lastname']);
                $mothersname = trim($customer['mothersname']);
            } else if ($customer['tid'] == '222') {
                $company = true;
                $company_name = mb_strtolower(trim($customer['firstname']), "UTF-8") . ' ' . mb_strtolower(trim($customer['lastname']), "UTF-8");
            }
            if ($address['cardtype'] != '') {
                $idType = $this->getCardTypeDescription($address['cardtype']);
            } else if ($customer['tid'] != '') {
                $idType = $this->getCardTypeDescription($customer['tid']);
            }
            $city = $this->getCity($address['city']);
            if ($address['coordenadas'] != '') {
                $coordenadas = explode(',', $address['coordenadas']);
                $latitud = $coordenadas[0];
                $longitud = $coordenadas[1];
            }
            $website = '';
            if ($customer['created_in']) {
                $website = strtoupper(trim(str_replace('Vista', '', $customer['created_in'])));
            }
            $dob = '';
            if ($customer['dob']) {
                $date = new DateTime($customer['dob']);
                $dob = $date->format('Ymd');
            }
            $celularClear = $this->clearStringPhone($address['celular']);
            if (preg_match("/^[3]{1}[0-9]{9}$/", $celularClear)) {
                $celular = '+57 ' . $celularClear;
            } else if (strlen($celularClear) >= 7 && strlen($celularClear) <= 9) {
                $celular = '+' . $celularClear;
            }
            $telephoneClear = $this->clearStringPhone($address['telephone']);
            if (strlen($telephoneClear) >= 7 && strlen($telephoneClear) <= 9) {
                $telephone = '+' . $telephoneClear;
            }
            $departamento = mb_strtolower($city['departamento'], "UTF-8");
            $city = mb_strtolower($city['municipio'], "UTF-8");
            $customer_data = array();
            $company_data = array();
            $area_data = array();
            $customer_commerce_data = array();
            $email = '';
            if (Zend_Validate::is($customer['email'], 'EmailAddress')) {
                $email = $customer['email'];
            }

            $firstname = substr($firstname, 0, 40);
            $middlename = substr($middlename, 0, 40);
            $lastname = substr($lastname, 0, 40);
            $mothersname = substr($mothersname, 0, 40);
            $company_name = substr($company_name, 0, 80);

     

            
           
            if ($this->_source == 'marketing') {
                if (!$company) {
                   $cardnumber= $this->eliminarletras($address['cardnumber']);
                    $customer_data = array(
                        'ID_ORIGIN' => 'MAGENTO',
                        'ID' => $customer['entity_id'],
                        'NAME_FIRST' => $this->clearStringCustomers(ucwords(mb_strtolower(trim($firstname), "UTF-8")), true),
                        'NAME_LAST' => $this->clearStringCustomers(ucwords(mb_strtolower(trim($lastname), "UTF-8")), true),
                        'TITLE_FT' => '',
                        'COUNTRY_FT' => 'CO',
                        'CITY1' => $this->clearStringCustomers(ucwords($city), true),
                        'POSTCODE1' => '',
                        'STREET' => '',
                        'HOUSE_NUM1' => '',
                        'SEX_FT' => $this->getGender($customer['gender']),
                        'CONSUMER_ACCOUNT_ID' => '',
                        'COMPANY_NAME' => '',
                        'COMPANY_ID_ORIGIN' => '',
                        'COMPANY_ID' => '',
                        'PAFKT_FT' => '',
                        'SMTP_ADDR' => $email,
                        'TELNR_LONG' => $telephone,
                        'TELNR_MOBILE' => $celular,
                        'DATE_OF_BIRTH' => $dob,
                        'ID_TW' => '',
                        'ID_FB' => '',
                        'ID_GP' => '',
                        'ID_ERP_CONTACT' => '',
                        'SMTP_ADDR_2' => '',
                        'SMTP_ADDR_3' => '',
                        'YY1_BCREATEDATE_ENH' => '',
                        'YY1_CCSTATUS_ENH' => '',
                        'YY1_CREDITAVAILAB_ENH' => '',
                        'YY1_CREDITTOTAL_ENH' => '',
			/*lbayona  ajuste 4823 hybris inicio*/
                        'YY1_IDNUMBER_ENH' => $this->clearStringCustomers($cardnumber),
                        /*lbayona  ajuste  4823 hybris-fin*/
			'YY1_LASTMODIFBRON_ENH' => '',
                        'YY1_MCREATIONDATE_ENH' => date('Ymd', Mage::getModel('core/date')->timestamp($customer['created_at'])),
                        'YY1_NEIGHBORHOOD_ENH' => substr($this->clearStringCustomers($address['observaciones']), 0, 40),
                        'YY1_OPENFREQUENCY_ENH' => '',
                        'YY1_STREET_CUSTOM_ENH' => $this->clearStringCustomers(substr($address['street'], 0, 60)),
                        'YY1_TYPEID_ENH' => $idType,
                        'YY1_DEPENDANTS_MPS' => '',
                        'YY1_EDUCLEVEL_MPS' => '',
                        'YY1_INCOME_MPS' => '',
                        'YY1_MIDDLENAME_MPS' => $this->clearStringCustomers(ucwords(mb_strtolower(trim($middlename)), "UTF-8"), true),
                        'YY1_MOTHERSNAME_MPS' => $this->clearStringCustomers(ucwords(mb_strtolower(trim($mothersname), "UTF-8")), true),
                        'YY1_NUMBERCARS_MPS' => '',
                        'YY1_PROFESSION_MPS' => '',
                        'YY1_TYPEHOUSING_MPS' => '',
                        'REGION_FT' => $this->clearStringCustomers(ucwords($departamento), true)
                    );
                                        
                    /*Carga el residual Marketing person*/
                    $this->_customerData[$customer['entity_id']] = array('id' => $customer['entity_id'], 'fecha' => $customer['updated_at']);
                    /*if ($this->_customerData == '') {
                        $this->_customerData = "'" . $customer['entity_id'] . "'";
                    } else {
                        $this->_customerData .= ",'" . $customer['entity_id'] . "'";
                    } */                                                                               
                } else if ($company_name != '') {
			/*lbayona  ajuste 4823 hybris inicio*/
                     $cardnumber= $this->eliminarletras($address['cardnumber']);
		     	/*lbayona   ajuste 4823 hybris fin */
                    $company_data = array(
                        'ID_ORIGIN' => 'MAGENTO',
                        'ID' => $customer['entity_id'],
                        'COMPANY_NAME' => $this->clearStringCustomers(ucwords(trim($company_name))),
                        'BRSCH_FT' => '',
                        'COUNTRY_FT' => 'CO',
                        'REGION_FT' => $this->clearStringCustomers(ucwords($departamento), true),
                        'CITY1' => $this->clearStringCustomers(ucwords($city), true),
                        'POSTCODE1' => '',
                        'STREET' => '',
                        'HOUSE_NUM1' => '',
                        'SMTP_ADDR' => $email,
                        'TELNR_LONG' => $telephone,
                        'TELNR_MOBILE' => $celular,
                        'ID_TW' => '',
                        'ID_FB' => '',
                        'ID_GP' => '',
                        'YY1_BCREATEDATE_ENH' => '',
                        'YY1_CCSTATUS_ENH' => '',
                        'YY1_CREDITAVAILAB_ENH' => '',
                        'YY1_CREDITTOTAL_ENH' => '',
			/*lbayona  ajuste  4823 hybris inicio*/
                        'YY1_IDNUMBER_ENH' => $this->clearStringCustomers($cardnumber),
                        /*lbayona   ajuste 4823 hybris fin */
			'YY1_LASTMODIFBRON_ENH' => '',
                        'YY1_MCREATIONDATE_ENH' => date('Ymd', Mage::getModel('core/date')->timestamp($customer['created_at'])),
                        'YY1_NEIGHBORHOOD_ENH' => '',
                        'YY1_OPENFREQUENCY_ENH' => '',
                        'YY1_STREET_CUSTOM_ENH' => $this->clearStringCustomers(substr($address['street'], 0, 60)),
                        'YY1_TYPEID_ENH' => $idType
                    );
                    
                    /*Carga el residual Marketing company*/
                    $this->_customerData[$customer['entity_id']] = array('id' => $customer['entity_id'], 'fecha' => $customer['updated_at']);
                    
                    /*if ($this->_customerData == '') {
                        $this->_customerData = "'" . $customer['entity_id'] . "'";
                    } else {
                        $this->_customerData .= ",'" . $customer['entity_id'] . "'";
                    }*/
                }
           } else if ($this->_source == 'commerce') {             
                $name = '';
                $surname = '';
                $name = $this->clearStringCustomers(ucwords(mb_strtolower(trim($firstname), "UTF-8")), true);
                if ($middlename != '') {
                    $name .= ' ' . $this->clearStringCustomers(ucwords(mb_strtolower(trim($middlename), "UTF-8")), true);
                }
                $surname = $this->clearStringCustomers(ucwords(mb_strtolower(trim($lastname), "UTF-8")), true);
                if ($mothersname != '') {
                    $surname .= ' ' . $this->clearStringCustomers(ucwords(mb_strtolower(trim($mothersname), "UTF-8")), true);
                }
                $idTypeCommerce = '';
                if ($address['cardtype'] != '') {
                    $idTypeCommerce = $this->getCardTypeCommerce($address['cardtype']);
                } else if ($customer['tid'] != '') {
                    $idTypeCommerce = $this->getCardTypeCommerce($customer['tid']);
                }
                 $cardnumber=$this->eliminarletras($address['cardnumber']);
                $customer_commerce_data = array(
                    'creationtime' => date('Ymd', Mage::getModel('core/date')->timestamp($customer['created_at'])),
                    'defaultPaymentAddress' => '',
                    'defaultShipmentAddress' => $this->clearStringCustomers(substr($address['street'], 0, 60)),
                    'name' => $this->clearStringCustomers(ucwords(mb_strtolower(trim($name), "UTF-8")), true),
                    'surname' => $this->clearStringCustomers(ucwords(mb_strtolower(trim($surname), "UTF-8")), true),
                    'originalUid' => $email,
                    'sessionCurrency' => 'COP',
                    'sessionLanguage' => 'ES',
                    'type' => 'REGISTERED',
                    'uid' => '',
                    'cellPhone' => $celular,
                    'nationalTypeID' => $idTypeCommerce,
                    'nationalID' => $this->clearStringCustomers($cardnumber),
                    'consentTC' => true,
                    'companyName' => $this->clearStringCustomers(ucwords(trim($company_name))),
                    'store' => $website
                );
                
                /*Carga el residual Commerce*/
                $this->_customers_processed[$customer['entity_id']] = array('id' => $customer['entity_id'], 'fecha' => $customer['updated_at']);
            }

            if ($customer['entity_id'] != '' && $website != '' && ($this->_source == 'marketing')) {
                try {
                    $ip = '';
                    if ($email != '') {
                        $sqlSubs = "SELECT subscriber_ip FROM `newsletter_subscriber` AS ns WHERE ns.subscriber_email ='" . $email . "';";
                        $result = $this->_connection->fetchAll($sqlSubs);
                        $resSubs = $result[0];
                        $ip = $resSubs['subscriber_ip'];
                    }
                    if ($celular && $celular != '') {
                        $area_data[0] = array(
                            'CONTACT_ORIGIN' => 'MAGENTO',
                            'CONTACT_ID' => $customer['entity_id'],
                            'ID_ORIGIN' => 'MOBILE',
                            'ID' => $celular,
                            'MKT_AREA_ID' => $website,
                            'MKT_PERM_COMM_MEDIUM' => 'PHONE',
                            'COMM_MEDIUM' => 'WEB',
                            'TIMESTAMP' => date('Ymd', Mage::getModel('core/date')->timestamp($customer['created_at'])),
                            'COMM_CAT_ID' => '',
                            'OPT_IN' => 'Y',
                            'YY1_IP_ADDRESS_MPM' => $ip,
                            'YY1_HABEASDATA_REP_MPM' => '101'
                        );
                        $area_data[1] = array(
                            'CONTACT_ORIGIN' => 'MAGENTO',
                            'CONTACT_ID' => $customer['entity_id'],
                            'ID_ORIGIN' => 'MOBILE',
                            'ID' => $celular,
                            'MKT_AREA_ID' => $website,
                            'MKT_PERM_COMM_MEDIUM' => 'SMS',
                            'COMM_MEDIUM' => 'WEB',
                            'TIMESTAMP' => date('Ymd', Mage::getModel('core/date')->timestamp($customer['created_at'])),
                            'COMM_CAT_ID' => '',
                            'OPT_IN' => 'Y',
                            'YY1_IP_ADDRESS_MPM' => $ip,
                            'YY1_HABEASDATA_REP_MPM' => '101'
                        );
                    }

                    
                    if ($email != '') {
                        $area_data[2] = array(
                            'CONTACT_ORIGIN' => 'MAGENTO',
                            'CONTACT_ID' => $customer['entity_id'],
                            'ID_ORIGIN' => 'EMAIL',
                            'ID' => $email,
                            'MKT_AREA_ID' => $website,
                            'MKT_PERM_COMM_MEDIUM' => 'EMAIL',
                            'COMM_MEDIUM' => 'WEB',
                            'TIMESTAMP' => date('Ymd', Mage::getModel('core/date')->timestamp($customer['created_at'])),
                            'COMM_CAT_ID' => '',
                            'OPT_IN' => 'Y',
                            'YY1_IP_ADDRESS_MPM' => $ip,
                            'YY1_HABEASDATA_REP_MPM' => '101'
                        );
                    }
                } catch (Exception $e) {
                    Mage::log("Error  - " . $e->getMessage(), null, 'scriptMarketingError.log');
                    Mage::log("--------Customer Data  - " . print_r($customer, true), null, 'scriptMarketingError.log');
                    Mage::log("     -----------Fin de Ejecucion " . get_class($this) . "-----------   ", null, 'scriptMarketingError.log');
                    echo "Error: " . $e->getMessage();
                }
            }                         
            
            //$fileName = 'customer_address.csv';            
            $current = date('d.m.Y', Mage::getModel('core/date')->timestamp(time()));            
            $fileName = 'InteractionContact_Person_MAGENTO_' . $current . '.csv';
            $fileNameCompany = 'InteractionContact_Company_MAGENTO_' . $current . '.csv';
            $fileNameArea = 'MarketingPermission_MAGENTO_' . $current . '.csv';
            $fileNameCommerce = 'y_customers_' . $current . '.csv';

            if ($this->_source == 'marketing') {
                if (count($customer_data) > 0) {
                    if ($this->_flag) {
                     

                        $this->putCsv($this->_dir, $fileName, $customer_data, $this->_flag, $this->getHeaderComments('person'), $this->_separator);
                        $this->_flag = false;
                    } else {
                        $this->putCsv($this->_dir, $fileName, $customer_data, $this->_flag, false, $this->_separator);
                    } 
                   
                   



                }
                if (count($company_data) > 0) {
                    if ($this->_flagCompany) {
                        $this->putCsv($this->_dir, $fileNameCompany, $company_data, $this->_flagCompany, $this->getHeaderComments('company'), $this->_separator);
                        $this->_flagCompany = false;
                    } else {
                        $this->putCsv($this->_dir, $fileNameCompany, $company_data, $this->_flagCompany, false, $this->_separator);
                    }
                 
                
                



                }
                if (count($area_data) > 0) {
                    foreach ($area_data as $area) {
                        if ($this->_flagArea) {
                            $this->putCsv($this->_dir, $fileNameArea, $area, $this->_flagArea, $this->getHeaderComments('permission'), $this->_separator);
                            $this->_flagArea = false;
                        } else {
                            $this->putCsv($this->_dir, $fileNameArea, $area, $this->_flagArea, false, $this->_separator);
                        }
                    }

                   

                         }
            } else if ($this->_source == 'commerce') {
                if (count($customer_commerce_data) > 0) {                    
                    $this->putCsv($this->_dirCommerce, $fileNameCommerce, $customer_commerce_data, $this->_flagCommerce, false, $this->_separator);
                    $this->_flagCommerce = false;
                }
            }

            unset($customer, $customer_data, $company_data, $area_data);
        }
        return;
    }                

}
