<?php

/* FE - Script para obtener informacion de marketing
 * @param Limit (type ini fin fecha) 
 * php /var/www/html/triggers/migracion/marketing/scriptMarketingExecuteAll.php all || orders || customers || subscribers 0 40000 ("sin limite" - -) 2017(4) || 2017-12-01(10)("sin a�o" -) (customers commerce || marketing || vacio(ambos))
 * Separador ~ sin "
 */
require_once dirname(__FILE__) . '/../../../app/Mage.php';
umask(0);

ini_set('display_errors', true);
ini_set('memory_limit', -1);
set_time_limit(0);
Mage::app('default')->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
require_once dirname(__FILE__) . '/scriptMarketingCustomers.php';
require_once dirname(__FILE__) . '/scriptMarketingOrders.php';
require_once dirname(__FILE__) . '/scriptMarketing.php';

print_r($argv);


$all='';
$from = '';
$to = '';
$fromYear = '';
$source = false; /* commerce, marketing, default ambos*/

/*all suscribers*/

if($argv[2]=="all"){
$all="all";
}

/*LIMIT*/
if ($argv[2] != '' && is_numeric($argv[2]) && $argv[2] != '-') {
    (string) $from = $argv[2];
}
if ($argv[3] != '' && is_numeric($argv[3]) && $argv[3] != '-') {
    (string) $to = $argv[3];
}

/*FECHA*/
if ($argv[4] != '' && $argv[4] != '-') {
    if (strlen($argv[4]) == 4) {
        (string) $fromYear = trim($argv[4]) . '-01-01 05:00:00';
    } else if (strlen($argv[4]) == 10) {
        (string) $fromYear = trim($argv[4]) . ' 05:00:00';
    }
}
Mage::log("     -----Inicia Ejecucion scriptMarketingAll-----------   ", null, 'scriptMarketingExecuteAll.log');
if ($argv[1] == '' || $argv[1] == 'all') {    
    $customers = new scriptMarketingCustomers();
    Mage::log("     -----------Inicia Ejecucion scriptMarketingCustomers-----------   ", null, 'scriptMarketingExecuteAll.log');
    $customers->getCustomers($from, $to, $fromYear);
    Mage::log("     -----------Finaliza Ejecucion scriptMarketingCustomers-----------   ", null, 'scriptMarketingExecuteAll.log');
    $orders = new scriptMarketingOrders();
    Mage::log("     -----------Inicia Ejecucion scriptMarketingOrders-----------   ", null, 'scriptMarketingExecuteAll.log');
    $orders->getOrders();
    Mage::log("     -----------Finaliza Ejecucion scriptMarketingOrders-----------   ", null, 'scriptMarketingExecuteAll.log');
    $marketing = new scriptMarketing();
    /*Mage::log("     -----------Inicia Ejecucion scriptMarketing-----------   ", null, 'scriptMarketingExecuteAll.log');
    $marketing->getSubscriber();
    Mage::log("     -----------Finaliza Ejecucion scriptMarketing-----------   ", null, 'scriptMarketingExecuteAll.log');*/
} else if ($argv[1] == 'subscribers') {
    $marketing = new scriptMarketing();
    Mage::log("     -----------Inicia Ejecucion scriptMarketing-----------   ", null, 'scriptMarketingExecuteAll.log');
    $marketing->getSubscriber($from, $to, $fromYear,$all);
    Mage::log("     -----------Finaliza Ejecucion scriptMarketing-----------   ", null, 'scriptMarketingExecuteAll.log');
} else if ($argv[1] == 'customers') {
    $customers = new scriptMarketingCustomers();
    Mage::log("     -----------Inicia Ejecucion scriptMarketingCustomers-----------   ", null, 'scriptMarketingExecuteAll.log');
    $source = 'marketing';
    /*if ($argv[5] != '' || $argv[5] != '-' && $argv[5] != '' && $argv[5] == 'marketing') {
        $source = $argv[5];
    }else{
        Mage::log("     --No viene parametro source--   ", null, 'scriptMarketingExecuteAll.log');
        echo "\n--No viene parametro source--\n";
        die();
    }*/
    $customers->getCustomers($from, $to, $fromYear, $source);
    Mage::log("     -----------Finaliza Ejecucion scriptMarketingCustomers-----------   ", null, 'scriptMarketingExecuteAll.log');
} else if ($argv[1] == 'orders') {
    $orders = new scriptMarketingOrders();
    Mage::log("     -----------Inicia Ejecucion scriptMarketingOrders-----------   ", null, 'scriptMarketingExecuteAll.log');
    $orders->getOrders($from, $to, $fromYear);
    Mage::log("     -----------Finaliza Ejecucion scriptMarketingOrders-----------   ", null, 'scriptMarketingExecuteAll.log');
} 
else {
    Mage::log("     --No viene parametro type--   ", null, 'scriptMarketingExecuteAll.log');
}
Mage::log("     -----Finaliza Ejecucion scriptMarketingAll-----------   ", null, 'scriptMarketingExecuteAll.log');