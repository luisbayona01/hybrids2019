<?php
/*app/corbeta/newlester/*/
/* FE - Script para obtener informacion de marketing
 * @param Limit (ini fin) 
 * php /var/www/html/triggers/migracion/marketing/scriptMarketingExecuteAll.php subscribers 0 40000 ("sin limite" - -) 2017(4) || 2017-12-01(10)
 * Separador ~ sin "
 */

class scriptMarketing {

    public $_limit = '';
    public $_stores = '1,3,6';
    public $_websites = '1,3,4';
    public $_flag = true;
    public $_municipios = array();
    public $_connection = '';
    public $_separator = '';
    public $_dir = '';
    public $_dirCommerce = '';
    public $_source = false;

    public function init() {
        $this->getConnection();
        $this->getMunicipios();
        $this->_flag = true;
        $this->_flagArea = true;
        $this->_separator = ';';
        $this->_fileNameTxt = 'customer_process.txt';
        $this->_commerceFileNameTxt = 'commerce_customer_process.txt';
        $this->_commerceFileOrders = 'commerce_orders_process.txt';
        $this->_commerceFileCustomers = 'commerce_customer_process.txt';
        $this->_dir = Mage::getBaseDir('var') . DS . 'export' . DS . 'migracion';
        $this->_dirCommerce = Mage::getBaseDir('var') . DS . 'export' . DS . 'migracion' . DS . 'commerce';
        $this->getCustomersProcessed();
    }


    public function getSubscriber($from, $to, $date = false,$all=false) {
         //$Daomail= new Daomail();
      
   
  

        $this->init();
        echo "     -----------Inicia Ejecucion " . get_class($this) . "-----------   \n";
        Mage::log("     -----------Inicia Ejecucion " . get_class($this) . "-----------   ", null, 'scriptMarketing.log');
        $this->_limit . "\n";
        if ($from != '' && $to != '') {
            $this->_limit = 'LIMIT ' . $from . ',' . $to;
        }

        $aditional = ' ';
        if ($date) {
        $aditional = " AND ns.subscriber_date >= '" . $date . "' ";
        //$aditional = " AND CAST(ns.subscriber_date AS DATE ) = '" .$fecha. "' ";
         
        }

if($all!=""){
$aditional="";
        }else 
        {
$currentdt = date('d-m-Y', Mage::getModel('core/date')->timestamp(time()));
$ayer=strtotime($currentdt."-24 hours");
   $fecha=date('Y-m-d',$ayer); 
 $aditional = " AND CAST(ns.change_status_at AS DATE ) = '" .$fecha. "' ";

 }
$res=array();

        try {
           $limit=$this->_limit; 
            $i = 1;


        $sql = " SELECT ns.subscriber_id ,ns.customer_id, ns.subscriber_date,ns.subscriber_ip,ns.subscriber_status ,ns.subscriber_email,ns.store_id FROM newsletter_subscriber ns  where subscriber_status IN(1,3)  ".$aditional . " group by ns.subscriber_email ORDER BY ns.subscriber_id ASC " . $this->_limit . ";";
       


             $deltav = $this->_connection->query($sql);

 while ($rows=$deltav->fetch()) {
          
          $res[]= $rows;  //print_r($rows);    
     
      } 
   






    

            Mage::log("#Datos a procesar: " . get_class($this) . " - " . count($res), null, 'scriptMarketing.log');
            if(count($res) == 0){
                echo "\n-----------No Data-----------";
                echo "\n-----------Fin de Ejecucion " . get_class($this) . "-----------\n";
                return false;
                Mage::log("     -----------No Data-----------   ", null, 'scriptMarketing.log');
                Mage::log("     -----------Fin de Ejecucion " . get_class($this) . "-----------   ", null, 'scriptMarketing.log');
            }

            /*bucle si   res  existe */
            if ($res) {
                $total = count($res);
                $factor = 1; 
                if ($total > 1000) {
                    $factor = 1000;
                }
                $intervalos = $total / $factor;
                $resto = $total % $factor;
                $interval = round($intervalos, 0, PHP_ROUND_HALF_UP);
                if ($resto > 0) {
                    $interval++;
                }
                $iteracion = 0;
                $limite = $factor;
                $contador = 1;
                for ($i = 1; $i <= $interval; $i++) {
                    for ($j = $iteracion; $j < $limite; $j++) {
                        if ($res[$j]) {
                           //echo $res[0][$j];  
                            //echo "\n" . $contador;
                            echo "\n" . ($contador / $total) * 100 . "%";
                            $this->setDataSubscriber($res[$j]);
                            $contador++; 
                        }
                    }
                    $limite += $factor;
                    $iteracion += $factor;
                }
            }
        } catch (Exception $e) {
            Mage::log("Error  - " . $e->getMessage(), null, 'scriptMarketingError.log');
            Mage::log("     -----------Fin de Ejecucion " . get_class($this) . "-----------   ", null, 'scriptMarketingError.log');
            echo "Error: " . $e->getMessage();
        }

                $current = date('d.m.Y', Mage::getModel('core/date')->timestamp(time()));
                $fileName = 'Person_Subscribers_MAGENTO_' . $current . '.csv';
                $fileNameArea = 'SubscribersPermission_MAGENTO_' . $current . '.csv';
        if(file_exists('/var/www/html/var/export/migracion/'.$fileName.'')){

         try{
                    $pname="Person_Subscribers_MAGENTO";
                    
                    

                echo exec( 'scp /var/www/html/var/export/migracion/'.$fileName.' marketing:/home/ubuntu/sandra/Interaction_Contact/1_MAGENTO');
         
                }catch(Exception $e){
                 Mage::log("Error  - " . $e->getMessage(), null, 'scriptMarketingError.log');
                }

        }else{
//echo "no lo creo".$fileName;

        }

 

 if(file_exists('/var/www/html/var/export/migracion/'.$fileNameArea.'')){

     try{
            
            $pmname="SubscribersPermission_MAGENTO_";
           
            
                  
           echo exec('scp /var/www/html/var/export/migracion/'.$fileNameArea.' marketing:/home/ubuntu/sandra/Interaction_Contact/1_MAGENTO');
 
        }catch(Exception $e){
           Mage::log("Error  - " . $e->getMessage(), null, 'scriptMarketingError.log');
        }
    }else{
//echo "no pdo crear".$fileNameArea;

    }


        Mage::log("     -----------Fin de Ejecucion " . get_class($this) . "-----------   ", null, 'scriptMarketing.log');
        echo "\n     -----------Fin de Ejecucion " . get_class($this) . "-----------   \n";
    }








    private function setDataSubscriber($subscriber) {
      



        if ($subscriber) {
           $subscriber['subscriber_status'];
            //exit();
            //$subscribe_status = '';
          /*$subscriber['subscriber_status'] == Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED*/
            if (($subscriber['subscriber_status'] == Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED) or($subscriber['subscriber_status'] =='3')) {
             
                /* $subscribe_status = 'Si';
                  } else if ($subscriber['subscriber_status'] == Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED) {
                  $subscribe_status = 'No';
                  } */
                $created = '';
              
                if ($subscriber['subscriber_date'] != '0000-00-00 00:00:00' && $subscriber['subscriber_date'] != '') {
                    $created = date('Ymd', Mage::getModel('core/date')->timestamp($subscriber['subscriber_date']));
                }
                $email = '';
                if (Zend_Validate::is($subscriber['subscriber_email'], 'EmailAddress')) {
                    $email = $subscriber['subscriber_email'];
                }
                $subscriber_data = array(
                    'ID_ORIGIN' => 'EMAIL',
                    'ID' => $email,
                    'NAME_FIRST' => '',
                    'NAME_LAST' => '',
                    'TITLE_FT' => '',
                    'COUNTRY_FT' => '',
                    'CITY1' => '',
                    'POSTCODE1' => '',
                    'STREET' => '',
                    'HOUSE_NUM1' => '',
                    'SEX_FT' => '',
                    'CONSUMER_ACCOUNT_ID' => '',
                    'COMPANY_NAME' => '',
                    'COMPANY_ID_ORIGIN' => '',
                    'COMPANY_ID' => '',
                    'PAFKT_FT' => '',
                    'SMTP_ADDR' => $email,
                    'TELNR_LONG' => '',
                    'TELNR_MOBILE' => '',
                    'DATE_OF_BIRTH' => '',
                    'ID_TW' => '',
                    'ID_FB' => '',
                    'ID_GP' => '',
                    'ID_ERP_CONTACT' => '',
                    'SMTP_ADDR_2' => '',
                    'SMTP_ADDR_3' => '',
                    'YY1_BCREATEDATE_ENH' => '',
                    'YY1_CCSTATUS_ENH' => '',
                    'YY1_CREDITAVAILAB_ENH' => '',
                    'YY1_CREDITTOTAL_ENH' => '',
                    'YY1_IDNUMBER_ENH' => '',
                    'YY1_LASTMODIFBRON_ENH' => '',
                    'YY1_MCREATIONDATE_ENH' => $created,
                    'YY1_NEIGHBORHOOD_ENH' => '',
                    'YY1_OPENFREQUENCY_ENH' => '',
                    'YY1_STREET_CUSTOM_ENH' => '',
                    'YY1_TYPEID_ENH' => '',
                    'YY1_DEPENDANTS_MPS' => '',
                    'YY1_EDUCLEVEL_MPS' => '',
                    'YY1_INCOME_MPS' => '',
                    'YY1_MIDDLENAME_MPS' => '',
                    'YY1_MOTHERSNAME_MPS' => '',
                    'YY1_NUMBERCARS_MPS' => '',
                    'YY1_PROFESSION_MPS' => '',
                    'YY1_TYPEHOUSING_MPS' => '',
                    'REGION_FT' => ''
                );
                if ($email != '') {
                    $storeName = '';
                    if ($subscriber['store_id']) {
                        $storeName = strtoupper($this->getStoreName($subscriber['store_id']));
                    }
                    $area_data = array(
                        'CONTACT_ORIGIN' => 'EMAIL',
                        'CONTACT_ID' => $email,
                        'ID_ORIGIN' => 'EMAIL',
                        'ID' => $email,
                        'MKT_AREA_ID' => $storeName,
                        'MKT_PERM_COMM_MEDIUM' => 'EMAIL',
                        'COMM_MEDIUM' => '',
                        'TIMESTAMP' => $created,
                        'COMM_CAT_ID' => '',
                        'OPT_IN' => '',
                        'YY1_IP_ADDRESS_MPM' => $subscriber['subscriber_ip'],
                        'YY1_HABEASDATA_REP_MPM' => '101'
                    );
                }

                $dir = Mage::getBaseDir('var') . DS . 'export' . DS . 'migracion';
                $current = date('d.m.Y', Mage::getModel('core/date')->timestamp(time()));
                $fileName = 'Person_Subscribers_MAGENTO_' . $current . '.csv';
                $fileNameArea = 'SubscribersPermission_MAGENTO_' . $current . '.csv';
               
          
                if (count($subscriber_data) > 0) {
                    if ($this->_flag) {
                        //echo"aqui"; 
                        $this->putCsv($dir, $fileName, $subscriber_data, $this->_flag, $this->getHeaderComments('person'), $this->_separator);
                        $this->_flag = false;
                    } else {
                        //echo"else";
                        $this->putCsv($dir, $fileName, $subscriber_data, $this->_flag, false, $this->_separator);
                    }
                  
                  
               
        
                }
                if (count($area_data) > 0) {
                    if ($this->_flagArea) {
                        $this->putCsv($dir, $fileNameArea, $area_data, $this->_flagArea, $this->getHeaderComments('permission'), $this->_separator);
                        $this->_flagArea = false;
                    } else {
                        $this->putCsv($dir, $fileNameArea, $area_data, $this->_flagArea, false, $this->_separator);
                    }
                /*scp /var/www/html/var/export/migracion/SubscribersPermission_MAGENTO_18.09.2018.csv marketing:/home/ubuntu/sandra/Interaction_Contact/1_MAGENTO*/

              
               
                }
                //$this->putCsv($dir, $fileName, $subscriber_data, $this->_flag);
                //$this->_flag = false;
            }
            unset($subscriber, $subscriber_data);
        }
        return;
    }

    public function putCsv($dir, $fileName, $data, $init = false, $headers = false, $sep = false, $mode = false) {

        if(!$mode){
            $mode = "w";
        }
        if (!is_dir($dir)) {//si el directorio no existe
            mkdir($dir, 0775, true); // crear directorio, permisos y recusivo
        }
        if ($sep) {
            $this->_separator = $sep;
        }
        $filePath = $dir . DS . $fileName;
        if (count($data) > 0) {
            $registro = '';
            if ($init) {
                $file = fopen($filePath, $mode);
                if($mode == "w"){
                    fputs($file, (chr(0xEF) . chr(0xBB) . chr(0xBF))); //add BOM to fix UTF-8 in Excel            
                }
                if ($headers) {
                    if($mode == "a"){
                        fwrite($file, "\n");
                    }
                    fwrite($file, $headers);
                }
                $cont = 0;
                foreach ($data as $k => $v) {
                    if ($cont == 0) {
                        $registro = $k;
                    } else {
                        $registro .= $this->_separator . $k;
                    }
                    $cont++;
                }
                fwrite($file, $registro);
                fwrite($file, "\n");
            } else {
                $file = fopen($filePath, "a");
            }
            //foreach ($data as $val) {
            $registro = '';
            $cont = 0;
            foreach ($data as $values) {
                print_r($values);
                if ($cont == 0) {
                    $registro = $values;
                } else {
                    $registro .= $this->_separator . $values;
                }
                $cont++;
            }

            fwrite($file, $registro);
            fwrite($file, "\n");
            //echo  $file;   
            //echo  $registro;
            //}
 
            fclose($file);
            unset($file, $registro, $init);
        }
    }

    public function getCardTypeDescription($typeId) {
     $attributeOptionSingle = Mage::getResourceModel('eav/entity_attribute_option_collection') ->setIdFilter($typeId)
->setStoreFilter(0)
->load()
->getFirstItem();

$typeIdata=$attributeOptionSingle->getdata();
$tipodoc= $typeIdata['value'];
 //print_r($attributeOptionSingle->getdata());

        switch ($tipodoc) {
          
           
            case 'Cédula de Ciudadania':
      
                $userdoctype = '70'; //CC
                break;
       
            case 'Cédula de Extranjería':
          
                $userdoctype = '71'; //CE
                break;
           
            case 'NIT':
            
                $userdoctype = '72'; //NIT
                break;
           
            case 'RUT':
           
                $userdoctype = '73'; //RUT
                break;
           
            case 'PASAPORTE':
         
                $userdoctype = '74'; //PASSPORT
                break;
            default:
                $userdoctype = '75'; //OTHER'
                break;
        }
        return $userdoctype;
    }

    public function getCardTypeCommerce($typeId) {

        switch ($typeId) {
            case '':
                $userdoctype = 'null';
                break;
            case '1321':
            case '220':
                $userdoctype = 'CC'; //CC
                break;
            case '1755':
            case '221':
                $userdoctype = 'CE'; //CE
                break;
            case '1320':
            case '222':
                $userdoctype = 'NIT'; //NIT
                break;
            case '6601':
            case '6655':
                $userdoctype = 'RUT'; //RUT
                break;
            case '6602':
            case '6656':
                $userdoctype = 'PASSPORT'; //PASSPORT
                break;
            default:
                $userdoctype = 'null'; //OTHER'
                break;
        }
        return $userdoctype;
    }
    
    public function getGender($genderId) {

        $gender = '';
        switch ($genderId) {
            case '1':
                $gender = 'M';
                break;
            case '2':
                $gender = 'F';
                break;
            default:
                break;
        }
        return $gender;
    }

    public function getMunicipios() {

        $sql = "SELECT dm.`id_dane`,dm.`municipio`,dcr.`default_name` FROM directory_municipio AS dm INNER JOIN directory_country_region AS dcr ON dm.`region_id` = dcr.`region_id`;";
        $this->_municipios = $this->_connection->fetchAll($sql);
        return true;
    }

    public function getCity($city) {
        foreach ($this->_municipios as $municipios) {
            if ($municipios['id_dane'] == $city) {
                $cityName = $this->quitarSaltos($municipios['municipio']);
                return array('municipio' => $cityName, 'departamento' => $municipios['default_name']);
            }
        }
        return array('municipio' => $city, 'departamento' => substr($city, 0, 2));
    }

    public function clearString($clear_string) {
        $search = array(
            '(', ')', '/', '\\', ';', '{', '}', '[', ']', '\'', '"', ',', ';', 'ç', ':', '%', '<', '>', '~', '¿', '?', '!', '¡', '-', '_', '+', '^', '`',
            '¬', '¨', '¨', 'À', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'Ê', 'Ë', 'Ì', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Û', 'Ü', 'Ý', 'Ÿ', 
            'Þ', 'ß', 'à', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'ê', 'ë', 'ì', 'î', 'ï', 'ð', 'ñ', 'ò', 'ô', 'õ', 'ö', 'ø', 'ù', 'û', 'ü', 'ý', 'þ', 'ÿ', 
            '&', '°', '', '®', '¤', '«' , '»', '¢', '¯', '¶', '™', '‹', '›', 'ª', '§', '¦', '£', 'º','¹', '³', '²', '¸', '¼', '½', '¾', '·', '×', '÷' ,
            '±', 'œ', 'Œ', 'Š', 'š', 'ƒ', '‘', '’', '‚', '“', '”', '„', '•', '‡', '†', '…', '‰', '€', '—', '–', '', '■', '▀', '┐', '≡', '‗', '┤', '┘',
            '■','�'
            );
        //$replace = array('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '','');
        $replace = '';
        $clear_string = $this->quitarSaltos($clear_string);
        $clear_string = str_replace($search, $replace, $clear_string);
        /*Caracter espacio no visible*/
        $clear_string = str_replace(' ', ' ', $clear_string); /*Caracter especial no visible*/
        return trim($clear_string);
    }
    
    public function clearStringPhone($clear_string) {
        $clear_string = $this->quitarSaltos($clear_string);
        $clear_string = preg_replace("/[^0-9]/",'',$clear_string);
        return $clear_string;
    }

    public function quitarSaltos($dato) {
        $dato = str_replace("\t", " ", trim($dato));
        $dato = str_replace("\r\n", " ", trim($dato));
        $dato = str_replace("\r", " ", trim($dato));
        $dato = str_replace("\n", " ", trim($dato));
        $dato = str_replace(PHP_EOL, " ", trim($dato));
        return $dato;
    }

    public function getConnection() {
        $this->_connection = Mage::getSingleton('core/resource')->getConnection('core_read');
    }

    public function getStoreName($store) {
        switch ($store) {
            case '1': return 'Ktronix';
            case '3': return 'Alkomprar';
            case '6': return 'Alkosto';
            default:
                break;
        }
        return '';
    }

    public function getHeaderComments($type = false) {
        $comments = '';
        if ($type == 'permission') {
            $comments .= "* User Instructions\n";
            $comments .= "* Enter the data for your upload directly below the header row starting at row 18.\n";
            $comments .= "* Do not delete the mandatory header row.\n";
            $comments .= "* You can define the order of the columns to meet your requirements.\n";
            $comments .= "* Do not enter any data in columns that have no attribute name in the header row.\n";
            $comments .= "* The column pair (ID_ORIGIN, ID) describes the contact facet for the permission and subscription\n";
            $comments .= "* The column pair (CONTACT_ORIGIN, CONTACT_ID) is needed to identify the contact. You must enter this data when the value in the ID column is shared by more than one\n";
            $comments .= "* If the column TIMESTAMP is left empty, the current date and time is set by the system. If the timestamp is set to a time in the past, the permission and subscription will not\n";
            $comments .= "* If the column COMM_CAT_ID is left empty, the system uploads the data as permission. If a value is entered in this column, the data is uploaded as subscription referring to\n";
            $comments .= "* Ensure that there are no empty rows above your data. Delete empty rows or enter an asterisk (*) at the start so that they are ignored for the upload.\n";
            $comments .= "* Comment rows are allowed only above the header row. They must start with an asterisk (*).\n";
            $comments .= "* As row separators, you can use <CRLF> for Windows systems and <CR> for Unix systems.\n";
            $comments .= "* As column separators, you can use a comma, semicolon, or tab.\n";
            $comments .= "* However, do not use separators (comma, semicolon, tab) in any text you enter in a cell.\n";
            $comments .= "* You can mask values using quotation marks or apostrophes. If you mask values, you must mask them in all columns.\n";
            $comments .= "* Save the file as a CSV file.\n";
            $comments .= "* For more information, see the application help.\n";
            $comments .= "*Consider the values for Habeas Data Repository  \n";
           
            $comments .= "* Contact Origin" . $this->_separator . "Contact ID" . $this->_separator . "ID Origin" . $this->_separator . "ID" . $this->_separator . "Marketing Area" . $this->_separator . "Outbound Communication Medium" . $this->_separator . "Communication Medium" . $this->_separator . "Timestamp" . $this->_separator . "Communication Category ID" . $this->_separator . "Opt in (Y/N)" . $this->_separator . "IP Address" . $this->_separator . "Habeas Data repository\n";
        } else
        if ($type == 'person') {
            $comments .= "* User Instructions\n";
            $comments .= "* Enter the data for your upload directly below the header row starting at row 22.\n";
            $comments .= "* Do not delete the mandatory header row which contains an attribute name per column. You can change or add columns to the header row if you need to.\n";
            $comments .= "* If you need to change or add columns, the following additional columns might be relevant: ID_ORIGIN, LANGUAGE_FT, MARITAL_STATUS_FT, BRSCH_FT (Industry), ABTNR_FT(Department),PAFKT_FT (Contact Function),LATITUDE, LONGITUDE, SRID\n";
            $comments .= "* You can define the order of the columns according to your requirements.\n";
            $comments .= "* Do not enter any data in columns that have no attribute name in the header row.\n";
            $comments .= "* Ensure that there are no empty rows above your data. Delete empty rows or enter an asterisk (*) at the start so that they are ignored for the upload.\n";
            $comments .= "* Comment rows are allowed only above the header row. They must start with an asterisk (*).\n";
            $comments .= "* Ensure that the header row contains no unknown attribute names (for example, due to typos). Unknown attribute names and the corresponding data are treated as comments and are ignored in the upload.\n";
            $comments .= "* As row separators, you can use <CRLF> for Windows systems and <CR> for Unix systems.\n";
            $comments .= "* As column separators, you can use a comma, semicolon, or tab.\n";
            $comments .= "* However, do not use separators (comma, semicolon, tab) in any text you enter in a cell.\n";
            $comments .= "* You can mask values using quotation marks or apostrophes. If you mask values, you must mask them in all columns.\n";
            $comments .= "* If you want to upload the origin of contact IDs, you can add a column ID_ORIGIN to the structure. If the ID_ORIGIN is filled, the ID column must be filled too.\n";
            $comments .= "* Attributes containing codes (e.g. ISO codes for COUNTRY) are not allowed. The corresponding free text attributes (e.g. COUNTRY_FT United States) are allowed.\n";
            $comments .= "* The recommended maximum size of each upload file is 10000 records.\n";
            $comments .= "* Save the file as a CSV file.\n";
            $comments .= "* For more information, see the application help.\n";
            $comments .= "*\n";
            $comments .= "* Origin of Contact" . $this->_separator . "Identifier" . $this->_separator . "First Name" . $this->_separator . "Last Name" . $this->_separator . "Title" . $this->_separator . "Country" . $this->_separator . "City" . $this->_separator . "City postal code" . $this->_separator . "Street" . $this->_separator . "House Number" . $this->_separator . "Gender (Male/Female)" . $this->_separator . "Consumer Account ID" . $this->_separator . "Company Name" . $this->_separator . "Origin of Company" . $this->_separator . "Company Identifier" . $this->_separator . "Function" . $this->_separator . "Email Address" . $this->_separator . "Phone Number (Landline)" . $this->_separator . "Cell Phone Number" . $this->_separator . "Date of Birth" . $this->_separator . "Twitter ID" . $this->_separator . "Facebook ID" . $this->_separator . "Google+ ID" . $this->_separator . "ERP Contact ID" . $this->_separator . "Email Address 2" . $this->_separator . "Email Address 3" . $this->_separator . "Bronto Creation Date" . $this->_separator . "Credit Card Status" . $this->_separator . "Credit Line Availability" . $this->_separator . "Credit Line Total" . $this->_separator . "Personal ID Number" . $this->_separator . "Last Modified in Bronto" . $this->_separator . "Magento Creation Date" . $this->_separator . "Neighborhood" . $this->_separator . "Open Frequency" . $this->_separator . "Dirección completa" . $this->_separator . "Type of Personal ID" . $this->_separator . "Dependants" . $this->_separator . "Educational Level" . $this->_separator . "Income" . $this->_separator . "Middle Name" . $this->_separator . "Mother's Name" . $this->_separator . "Number of cars" . $this->_separator . "Profession" . $this->_separator . "Type of Housing" . $this->_separator . "Region\n";
        } else
        if ($type == 'company') {
            $comments .= "* User Instructions\n";
            $comments .= "* Enter the data for your upload directly below the header row starting at row 21.\n";
            $comments .= "* Do not delete the mandatory header row which contains an attribute name per column. You can change or add columns to the header row if you need to.\n";
            $comments .= "* You can define the order of the columns according to your requirements.\n";
            $comments .= "* Do not enter any data in columns that have no attribute name in the header row.\n";
            $comments .= "* Ensure that there are no empty rows above your data. Delete empty rows or enter an asterisk (*) at the start so that they are ignored for the upload.\n";
            $comments .= "* Comment rows are allowed only above the header row. They must start with an asterisk (*).\n";
            $comments .= "* Ensure that the header row contains no unknown attribute names (for example, due to typos). Unknown attribute names and the corresponding data are treated as comments and are ignored in the upload.\n";
            $comments .= "* As row separators, you can use <CRLF> for Windows systems and <CR> for Unix systems.\n";
            $comments .= "* As column separators, you can use a comma, semicolon, or tab.\n";
            $comments .= "* However, do not use separators (comma, semicolon, tab) in any text you enter in a cell.\n";
            $comments .= "* You can mask values using quotation marks or apostrophes. If you mask values, you must mask them in all columns.\n";
            $comments .= "* If you want to upload the origin of contact IDs, you can add a column ID_ORIGIN to the structure. If the ID_ORIGIN is filled, the ID column must be filled too.\n";
            $comments .= "* Attributes containing codes (e.g. ISO codes for COUNTRY) are not allowed. The corresponding free text attributes (e.g. COUNTRY_FT United States) are allowed.\n";
            $comments .= "* The recommended maximum size of each upload file is 10000 records.\n";
            $comments .= "* Save the file as a CSV file.\n";
            $comments .= "* For more information, see the application help.\n";
            $comments .= "*\n";
            $comments .= "* Origin of Company" . $this->_separator . "Identifier" . $this->_separator . "Company Name" . $this->_separator . "Customer Industry" . $this->_separator . "Country" . $this->_separator . "Region" . $this->_separator . "City" . $this->_separator . "City postal code" . $this->_separator . "Street" . $this->_separator . "House Number" . $this->_separator . "Email Address" . $this->_separator . "Phone Number (Landline)" . $this->_separator . "Cell Phone Number" . $this->_separator . "Twitter ID" . $this->_separator . "Facebook ID" . $this->_separator . "Google+ ID" . $this->_separator . "Bronto Creation Date" . $this->_separator . "Credit Card Status" . $this->_separator . "Credit Line Availability" . $this->_separator . "Credit Line Total" . $this->_separator . "Personal ID Number" . $this->_separator . "Last Modified in Bronto" . $this->_separator . "Magento Creation Date" . $this->_separator . "Neighborhood" . $this->_separator . "Open Frequency" . $this->_separator . "Dirección completa" . $this->_separator . "Type of Personal ID\n";
        } else
        if ($type == 'orders') {
            $comments .= "* User Instructions:\n";
            $comments .= "* Row 17 displays the Custom Business Object ID. Do not delete this row.\n";
            $comments .= "* Row 19 displays the attribute descriptions per column.\n";
            $comments .= "* Row 20 displays the attribute name per column. Do not delete this header row.\n";
            $comments .= "* Row 21 displays the data type and length information for the attribute name per column. Enter the data for your upload directly starting at this row.\n";
            $comments .= "* Ensure that there are no empty rows above your data. Delete empty rows or enter an asterisk (*) at the start so that they are ignored for the upload.\n";
            $comments .= "* Comment rows are allowed only above the header row. They must start with an asterisk (*).\n";
            $comments .= "* Ensure that the header row contains no unknown attribute names (for example due to typos). Unknown attribute names and the corresponding data are treated as comments and are ignored in the upload.\n";
            $comments .= "* As row separators- you can use <CRLF> for Windows systems and <CR> for Unix systems.\n";
            $comments .= "* As column separators- you can use a comma / semicolon / tab.\n";
            $comments .= "* However do not use separators (comma / semicolon / tab) in any text you enter in a cell.\n";
            $comments .= "* You can mask values using quotation marks or apostrophes. If you mask values- you must mask them in all columns.\n";
            $comments .= "* The recommended maximum size of each upload file is 10000 records.\n";
            $comments .= "* Save the file as a CSV file.\n";
            $comments .= "* For more information, see the application help.\n";
            //$comments .= "*Origin ID ID  TimeStamp and Invoce Number are mandatory\n";
            $comments .= "*\n";
            //$comments .= "*Custom Business Object ID:YY1_CUSTOM_SALESORDER\n";
            $comments .= "*Custom Business Object ID:YY1_CBO_SALES_ORDER_V2\n";
            //$comments .= "*Consider:\n";            
            //$comments .= "*Consider:\n";
            $comments .= "*\n";
            $comments .= "*Communication Medium" . $this->_separator . "Credit Card Type" . $this->_separator . "Delivery method" . $this->_separator . "ID" . $this->_separator . "ID Origin" . $this->_separator . "Instalment" . $this->_separator . "Costum Interaction Type" . $this->_separator . "Invoice Number" . $this->_separator . "Order Date" . $this->_separator . "Order Number" . $this->_separator . "Payment bank" . $this->_separator . "Payment method" . $this->_separator . "Product Name" . $this->_separator . "Product SKU" . $this->_separator . "Product Category" . $this->_separator . "Units" . $this->_separator . "Created By" . $this->_separator . "Created On" . $this->_separator . "Last Changed By" . $this->_separator . "Last Changed On" . $this->_separator . "Time Stamp" . $this->_separator . "Total Order_V" . $this->_separator . "TOTAL_ORDER_C\n";
        }
        if ($comments != '') {
            return $comments;
        }
        return false;
    }

    public function getCsvData($file = false, $delimiter = '', $enclosure = '') {
        
        $data = array();
        if (!file_exists($file)) {
            Mage::log("File : " . $file . " no existe - " . get_class($this) . "-----------   ", null, 'scriptMarketingError.log');
            echo "File : " . $file . " no existe - " . get_class($this) . "-----------   \n";
            return false;
        }
        echo "\nEntra getCsvData: " . $file;
        $fh = fopen($file, 'r');
        if ($fh) {
            while ($rowData = fgetcsv($fh, 0, ';', '')) {
                $data[] = count($rowData);
                echo "\ncontar: " . $data;
            }
            fclose($fh);
        } else {
            return false;
        }
        count($data);
        fclose($fh);
        return $data;
    }
    
    public $_customers_processed = array();
    public $_corte = '';
    public $_customer_corte = '';
    
    public function getCustomersProcessed($source = false){
        $this->_corte = '';        
        $this->_ordersData = '';
        if ($this->_source == 'commerce' && !$source) {
            $dirCommerce = $this->_dirCommerce . DS . $this->_commerceFileNameTxt;
            if (filesize($dirCommerce)) {
                $file = file($dirCommerce);
                if (strlen($file[1]) > 0) {
                    $this->_customers_processed = unserialize($file[1]);
                    if (strlen($file[0]) > 0) {
                        $this->_customer_corte = $this->quitarSaltos($file[0]);
                    }
                }
            }
        }else if ($this->_source == 'marketing' && !$source) {
            $dir = $this->_dir . DS . $this->_fileNameTxt;
            if (filesize($dir)) {
                $file = file($dir);
                if (strlen($file[1]) > 0) {
                    $this->_customerData = unserialize($file[1]);
                    if (strlen($file[0]) > 0) {
                        $this->_corte = $this->quitarSaltos($file[0]);
                    }
                }
            }
        }else if($source == 'orders'){            
            $dir = $this->_dir . DS . $this->_fileOrders;
            if (filesize($dir)) {
                $file = file($dir);
                if (strlen($file[1]) > 0) {
                    $this->_ordersData = unserialize($file[1]);
                    if (strlen($file[0]) > 0) {
                        $this->_corte = $this->quitarSaltos($file[0]);
                    }
                }
            }
        } else if($source == 'commerce_orders'){            
            $dir = $this->_dirCommerce . DS . $this->_commerceFileOrders;
            if (filesize($dir)) {
                $file = file($dir);
                if (strlen($file[1]) > 0) {
                    $this->_ordersData = unserialize($file[1]);
                    if (strlen($file[0]) > 0) {
                        $this->_corte = $this->quitarSaltos($file[0]);
                    }
                }
            }
        } else if($source == 'commerce_orders_test'){            
            $dirCommerce = $this->_dirCommerce . DS . 'commerce_customer_test_process.txt';
            if (filesize($dirCommerce)) {
                $file = file($dirCommerce);
                if (strlen($file[1]) > 0) {
                    $this->_customers_processed = unserialize($file[1]);
                    if (strlen($file[0]) > 0) {
                        $this->_customer_corte = $this->quitarSaltos($file[0]);
                    }
                }
            }
        }
        
        //return array('customers' => $customers, 'corte' => $this->quitarSaltos($file[0]));
        return;
    }
 



    public function putTxt($dir, $fileName,$data,$current = false) {
        if (!is_dir($dir)) {//si el directorio no existe
            mkdir($dir, 0775, true); // crear directorio, permisos y recusivo
        }
        $filePath = $dir . DS . $fileName;
        if ($data) {
            $file = fopen($filePath, "w");
            if ($current){
                fwrite($file, $current);
                fwrite($file, "\n");
            }
            if (is_array($data)) {
                foreach ($data as $row) {
                    fwrite($file, $row);
                    fwrite($file, "\n");
                }
            } else {
                fwrite($file, $data);
            }
        }
        fclose($file);
        unset($file, $row);
    }
    
    public function getCityByName($name = false, $region = false) {
        try{
            if ($name && $region) {
                $sql = "SELECT m.`id_dane` FROM `directory_municipio` AS m WHERE m.`municipio` LIKE '%" . $name . "%' AND m.region_id = '" . $region . "';";
                //Mage::log("     ---sql " . $sql . "-----------   ", null, 'scriptOrders.log');
                $res = $this->_connection->fetchOne($sql);
                if ($res != '') {
                    return $res;
                }
            }
            return '';
        } catch (Exception $e) {
            Mage::log("Error  - " . $e->getMessage(), null, 'scriptOrders.log');
            Mage::log("     ----------- " . get_class($this) . "-----------   ", null, 'scriptOrders.log');
            echo "Error: " . $e->getMessage();
            return '';
        }
        
    }
    
    public function getRegionCode($region = false) {
        try{
            if ($region) {
                $sql = "SELECT r.`code` FROM `directory_country_region` AS r WHERE r.`region_id` = '" . $region . "';";
                //Mage::log("     ---sql " . $sql . "-----------   ", null, 'scriptOrders.log');
                $res = $this->_connection->fetchOne($sql);
                if ($res != '') {
                    return $res;
                }
            }
            return '';
        } catch (Exception $e) {
            Mage::log("Error  - " . $e->getMessage(), null, 'scriptOrders.log');
            Mage::log("     ----------- " . get_class($this) . "-----------   ", null, 'scriptOrders.log');
            echo "Error: " . $e->getMessage();
            return '';
        }
        
    }
    
    public function clearStringCustomers($clear_string, $person = false) {
        $clear_string = $this->quitarSaltos($clear_string);
        if ($person) {
            $search = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
            $replace = array('', '', '', '', '', '', '', '', '', '');
            $clear_string = str_replace($search, $replace, $clear_string);
            $clear_string = str_replace('&', '', $clear_string);
        }
        $search = array('..',' ..', ' .', '/', '\\', ';', ',', 'D.c.');
        $replace = array('','', '', ' ', ' ', ' ', ' ', 'D.C.');
        $clear_string = str_replace($search, $replace, $clear_string);
        $clear_string = $this->clearString($clear_string);
        return $clear_string;
    }

}